# Assignment 2: Create a Dynamic Webpage in JS
In this assignment I have created a webpage using html, js and css.
This webpage allows a user to browse computers, click a work button to earn money,
transfer that money to their bank, loan money and buy a computer.

## 1)
Webstorm was used with live debugging aswell as chromes developer tools.

## 2)
The provided wireframe was followed at the best of my abilities

## 3)
Basic html was written aswell as basic css to add some colors and rounded corners.
Positioning was a bit of a challenge, especially to make the page as dynamic as possible with resizing.
A framework such as bootstrap would be very beneficial for this and would most likely make the css work
both easier and imensely better, but for the sake of my own skillset i tried to remember and relearn
some basic css skills.

## 4)
JS was used to get data and images from the API to provide the different laptops with names, images, speccs and descriptions.
This fills the select on load, aswell as the preview window, and changes on using the select.
It was also used to allow the buttons and various balances to function as intended.

## 5)
As far as i know i couldnt get the link on gitlab to work, the pipeline passed but the link:
https://Steffentrovik.gitlab.io/assignment4 still gives 404. Would love to know what might be the problem here. Varified my account, and even tried moving the files up a tier.

So, the three files: index.js, index.html and index.css have been uploaded to my private github repo https://github.com/sat90/sat90.github.io
and is therefore hosted on https://sat90.github.io

## Contributers

* Steffen Trovik
